import os
import pathlib

BASE_DIR = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SECRET_KEY = os.environ.get("SECRET_KEY") or "RgUL8?~&EUZV{6c9"

    SQLALCHEMY_DATABASE_URI = os.environ.get("FLASK_DATABASE_URL") or "sqlite:///" + os.path.join(
        BASE_DIR, "debug_app.db"
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    INIT_DB_DIR = os.path.join(BASE_DIR, "db_init/files/")

    STATIC_FOLDER = os.path.join(BASE_DIR, "app/common/static/")

    pathlib.Path(
        os.path.join(STATIC_FOLDER, "upload/")
    ).mkdir(parents=True, exist_ok=True)
