from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField, SubmitField, SelectField
from wtforms.validators import DataRequired, Email, EqualTo, ValidationError

from app.models import User, Exploitation


class RegistrationForm(FlaskForm):
    email = StringField("Adresse email", validators=[DataRequired(), Email()])
    name = StringField("Prénom", validators=[DataRequired()])
    surname = StringField("Nom", validators=[DataRequired()])
    password = PasswordField("Mot de passe", validators=[DataRequired()])
    password2 = PasswordField(
        "Vérification du mot de passe", validators=[DataRequired(), EqualTo("password")]
    )

    exploitation_name = StringField(
        "Nom de l'exploitation", validators=[DataRequired()]
    )
    exploitation_type = SelectField(
        "Type d'exploitation",
        choices=[
            ("scea", "Société civile d'exploitation agricole (SCEA)"),
            ("gaec", "Groupement agricole d'exploitation en commun (GAEC)"),
            ("earl", "Exploitation agricole à responsabilité limitée (EARL)"),
            ("sep" , "Société de fait / Société en participation (SEP)")
        ],
        validators=[DataRequired()],
    )
    submit = SubmitField("Créer mon compte")

    def validate_name(self, name):
        user = User.query.filter_by(name=name.data).first()
        if user is not None:
            raise ValidationError("Ce pseudo est déjà utilisé.")

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError("Cette adresse email est déjà utilisée.")

    def validate_exploitation_name(self, exploitation_name):
        exploitation = Exploitation.query.filter_by(name=exploitation_name.data).first()
        if exploitation is not None:
            raise ValidationError("Cette exploitation existe déjà.")
