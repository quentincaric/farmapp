from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_login import current_user, login_required, login_user, logout_user
from werkzeug.urls import url_parse

from app.extensions import sql_db
from app.models import User, Exploitation

from .forms import LoginForm, RegistrationForm

AUTHENTIFICATION_BP = Blueprint(
    "authentification", 
    __name__, 
    template_folder="templates/",
    static_folder="static/",
    static_url_path="/static/authentification/",
)


@AUTHENTIFICATION_BP.route("/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for("home.home"))

    login_form = LoginForm()
    if login_form.validate_on_submit():
        user = User.query.filter_by(name=login_form.name.data).first()
        if user is None or not user.check_password(login_form.password.data):
            flash("Utilisateur et/ou mot de passe incorrect(s)", "danger")
            return redirect(url_for("authentification.login"))

        login_user(user, remember=login_form.remember_me.data)

        next_page = request.args.get("next")
        if not next_page or url_parse(next_page).netloc != "":
            next_page = url_for("home.home")

        return redirect(next_page)

    return render_template("login.html", title="Sign In", form=login_form)


@AUTHENTIFICATION_BP.route("/register", methods=["GET", "POST"])
def register():
    if current_user.is_authenticated:
        return redirect(url_for("home.home"))

    lat, long = (
        request.args.get("latitude", type=float),
        request.args.get("longitude", type=float),
    )

    form = RegistrationForm()
    if request.method == "POST":
        exploitation = Exploitation(
            type=form.exploitation_type.data,
            name=form.exploitation_name.data,
            lat=lat,
            long=long,
        )
        sql_db.session.add(exploitation)
        sql_db.session.commit()
        flash("Exploitation créée", "success")

        user = User(
            email=form.email.data,
            name=form.name.data,
            surname=form.surname.data,
            exploitation_id=exploitation.id,
            type="admin",
        )
        user.set_password(form.password.data)
        sql_db.session.add(user)
        sql_db.session.commit()
        flash("Utilisateur créé", "success")

        return redirect(url_for("authentification.login"))

    return render_template("register.html", title="Register", form=form)


@AUTHENTIFICATION_BP.route("/logout")
def logout():
    logout_user()
    return redirect(url_for("authentification.login"))
