from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_moment import Moment
from flask_sqlalchemy import SQLAlchemy

# pylint: disable=invalid-name


bootstrap = Bootstrap()
login = LoginManager()
login.login_view = "authentification.login"
login.login_message = "Veuillez vous connecter pour accéder à cette page"
login.login_message_category = "danger"
migrate = Migrate()
sql_db = SQLAlchemy()
moment = Moment()
