from flask_login import UserMixin
from werkzeug.security import check_password_hash, generate_password_hash

from app.extensions import login, sql_db


class User(UserMixin, sql_db.Model):
    id = sql_db.Column(sql_db.Integer, primary_key=True)
    email = sql_db.Column(sql_db.String(120), unique=True)
    name = sql_db.Column(sql_db.String(64))
    surname = sql_db.Column(sql_db.String(64))
    password_hash = sql_db.Column(sql_db.String(128))
    type = sql_db.Column(sql_db.String(64))

    exploitation_id = sql_db.Column(
        sql_db.Integer, sql_db.ForeignKey("exploitation.id")
    )
    task_edited_list = sql_db.relationship(
        "Task", backref="editor", lazy=True
    )

    def get_id(self):
        return self.id

    def __repr__(self):
        return "<User {}>".format(self.name)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


@login.user_loader
def load_user(id):
    return User.query.get(int(id))
