from app.extensions import sql_db


class Field(sql_db.Model):
    id = sql_db.Column(sql_db.Integer, primary_key=True, autoincrement=True)
    name = sql_db.Column(sql_db.String(64), index=True)
    superficie = sql_db.Column(sql_db.Integer)
    type = sql_db.Column(sql_db.String(64))
    lat = sql_db.Column(sql_db.Float, )
    long = sql_db.Column(sql_db.Float)

    exploitation_id = sql_db.Column(
        sql_db.Integer, sql_db.ForeignKey("exploitation.id")
    )
    task_list = sql_db.relationship(
        "Task", backref="field", lazy=True
    )

    def get_default_form_values(self):
        default_form_values_dict = {}
        for var in ["type", "name", "superficie", "lat", "long"]:
            default_form_values_dict[var] = getattr(self, var)
        return default_form_values_dict
