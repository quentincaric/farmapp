from app.extensions import sql_db

class Task(sql_db.Model):
    date = sql_db.Column(sql_db.Date(), primary_key=True)
    field_id = sql_db.Column(
        sql_db.Integer, sql_db.ForeignKey("field.id"), primary_key=True
    )
    editor_id = sql_db.Column(
        sql_db.Integer, sql_db.ForeignKey("user.id")
    )
    is_done = sql_db.Column(sql_db.Boolean, default=False)
    comment = sql_db.Column(sql_db.String)