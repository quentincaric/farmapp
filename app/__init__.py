
from flask import Flask

from config import Config

from .extensions import bootstrap, login, migrate, moment, sql_db

from .home import HOME_BP
from .authentification import AUTHENTIFICATION_BP


def create_app(config_class=Config):
    app = Flask(__name__, template_folder="common/templates")

    app.config.from_object(config_class)
    bootstrap.init_app(app)
    sql_db.init_app(app)
    migrate.init_app(app, sql_db, render_as_batch=True)
    login.init_app(app)
    moment.init_app(app)

    app.register_blueprint(HOME_BP)
    app.register_blueprint(AUTHENTIFICATION_BP)

    return app
