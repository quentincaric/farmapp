from flask_wtf import FlaskForm
from wtforms import SelectField, DateField, TextAreaField, BooleanField
from wtforms.validators import DataRequired


class CreateTaskForm(FlaskForm):
    date = DateField("Date", validators=[DataRequired()])
    field_id = SelectField("Nom du terrain", validators=[DataRequired()])
    comment = TextAreaField("Description", validators=[DataRequired()])
    is_done = BooleanField("Tâche terminée")

    def __init__(self, current_user, *args, **kwargs):
        super(CreateTaskForm, self).__init__(*args, **kwargs)
        self.field_id.choices = [
            (field.id, field.name) for field in current_user.exploitation.field_list
        ]
