from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, FloatField, IntegerField, SelectField
from wtforms.validators import DataRequired


class CreateFieldForm(FlaskForm):
    type = SelectField("Type de terrain", choices=[
        ("culture", "Culture"),
        ("creeding", "Elevage"),
        ("others", "Autres"),
    ])
    name = StringField("Nom du terrain", validators=[DataRequired()])
    superficie = IntegerField("Superficie du terrain")
    lat = FloatField("Latitude", validators=[DataRequired()])
    long = FloatField("Longitude", validators=[DataRequired()])
    submit = SubmitField("Valider")
