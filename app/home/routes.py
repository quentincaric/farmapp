from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_login import current_user, login_required

from app.models import Field, Task

from .forms import CreateFieldForm, CreateTaskForm
from app.extensions import sql_db

HOME_BP = Blueprint(
    "home", 
    __name__, 
    template_folder="templates/",     
    static_folder="static/",
    static_url_path="/static/home/",
)


@HOME_BP.route("/", methods=["GET", "POST"])
@HOME_BP.route("/home", methods=["GET", "POST"])
@login_required
def home():
    exploitation = current_user.exploitation
    field_list = exploitation.field_list

    return render_template(
        "home.html",
        field_list=field_list,
        exploitation=exploitation,
    )


@HOME_BP.route("/terrain/creation/", methods=["GET", "POST"])
@HOME_BP.route("/terrain/maj/<field_id>/", methods=["GET", "POST"])
@login_required
def create_update_field(field_id=None):
    current_field = None if not field_id else Field.query.get(field_id)
    form = (
        CreateFieldForm(
            lat=request.args.get("latitude", type=float),
            long=request.args.get("longitude", type=float)
        )
        if current_field is None
        else CreateFieldForm(**current_field.get_default_form_values())
    )

    if form.validate_on_submit():
        new_field = Field(
            exploitation_id=current_user.exploitation_id,
            type=form.type.data,
            superficie=form.superficie.data,
            name=form.name.data,
            lat=form.lat.data,
            long=form.long.data,
        )
        if current_field:
            new_field.id = current_field.id

        field_upsert = sql_db.session.merge(new_field)
        sql_db.session.add(field_upsert)
        sql_db.session.commit()
        flash("Terrain mis à jour" if current_field else "Terrain ajouté", "success")
        return redirect(url_for("home.home"))

    return render_template("create_update_field.html", form=form, current_field=current_field)


@HOME_BP.route("/terrain/delete/<field_id>/", methods=["GET", "POST"])
@login_required
def delete_field(field_id):
    field = Field.query.get(field_id)
    sql_db.session.delete(field)
    sql_db.session.commit()
    flash(f"Terrain '{field.name}' supprimé", "success")
    return redirect(url_for("home.home"))


@HOME_BP.route("/terrain/task/", methods=["GET", "POST"])
@HOME_BP.route("/terrain/task/<field_id>/", methods=["GET", "POST"])
def show_tasks(field_id=None):
    field_list = Field.query.filter_by(
        exploitation_id=current_user.exploitation_id
    )
    field_id_list = [field.id for field in field_list]
    tasks_list = Task.query.filter(
        Task.field_id.in_(field_id_list)).order_by("date")

    form = CreateTaskForm(current_user=current_user)
    if form.validate_on_submit():
        task = Task(
            date=form.date.data,
            field_id=form.field_id.data,
            editor_id=current_user.id,
            comment=form.comment.data,
        )
        task_upsert = sql_db.session.merge(task)
        sql_db.session.add(task_upsert)
        sql_db.session.commit()

        form = CreateTaskForm(current_user=current_user)
        return redirect(url_for("home.show_tasks", tasks_list=tasks_list, form=form))

    return render_template("tasks.html", tasks_list=tasks_list, form=form)
