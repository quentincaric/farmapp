FROM python:3.11 as app

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
RUN pip install gunicorn pymysql cryptography

COPY app app
COPY config.py config.py
COPY app.py app.py
COPY migrations migrations
COPY entrypoint.bat entrypoint.bat
COPY data data

ENV FLASK_DATABASE_URL "sqlite:///C:/data/test.db"

EXPOSE 5000
ENTRYPOINT ["entrypoint.bat"]