from app import create_app
from app.extensions import sql_db

app = create_app()  # pylint: disable=invalid-name


@app.shell_context_processor
def make_shell_context():
    return {"sql_db": sql_db}
